Introducción
Proyecto en Unity de un juego de aventuras en el que 2 personajes(Humano y CPU)
compiten mediante insultos.

MODO DE JUEGO
-Comenzamos defendiéndonos de insultos de la CPU.Para ello escogemos una 
respuesta que nos parezca correcta para su insulto.
-Si acertamos la respuesta correcta:
	-Pasamos al ataque y se nos añade un punto a nuestro marcador.
-Si fallamos:
	-Seguimos en defensa y la CPU nos insulta de nuevo.

Este proceso se repite 3 rondas, tras las cuales en caso de ganar:
		-El camarero comenta: "A esta invito yo"
		-Aparece un rótulo de "Victoria" en la esquina inferior derecha.
Si perdemos el camarero nos dirá "Casi mejor que te vayas marchando, campeón".

ESTRUCTURA DEL PROYECTO

		Assets
		  |	
		  |-Resources	
		  |	
		  |-Scenes
		  |	
		  |-Scripts
		  |	
		  |botón_dialogo_script
		  |	
		  |boton_dialogo_bueno	
		    |- Text_button_dialogo	
		 	
Resources: contiene los archivos JSON desde los cuales cargamos insultos y res-
-puestas de la partida, además de una serie de sprites e imágenes que se emplean
en el juego. Algunas de ellas no han podido implementarse, pero de todos modos
se mantienen en la carpeta para futuras actualizaciones.

Scenes: contiene las escenas que componen el juego, además de una escena 'test' 
que se emplea para eso mismo, testear.

Scripts: contiene los scripts que emplean los GameObjects y las clases de datos
usadas en el proyecto, excepto boton_dialogo_script, que se encuentra en la 
carpeta Assets por motivos de visibilidad.

boton_dialogo_bueno & Text_button_dialogo: componentes del prefab que aparece
como boton clicable durante la ejecución del juego.

boton_dialogo_script: Script que emplea el prefab mencionado para devolver el 
valor seleccionado y aplicarlo al juego.



EXPLICACION DE CLASES, SCRIPTS Y DISEÑO

En el proyecto se han creado una serie de clases para poder almacenar y 
transferir datos cómodamente:

----------------Clases estructurales----------------
Prácticamente sin funcionalidad además de transferir datos.

-----
datos_partida: Una clase que agrupa todos los datos que componen la partida de 
forma completa. Tiene los atributos:
	-lista_insultos_master: Una lista de insultos, en este caso solo los insul-
	                       -tos sacados del JSON. Su tipo es lista_ins_res, el 
	                       cual se creó especificamente para sacar valores del JSON*.
	-lista_respuestas_master: Igual que la lista_insultos_master pero contiene 
	                          las respuestas sacadas del JSON con respuestas.
	-player1: Datos de nuestro personaje durante la partida, de tipo 
	          personaje_base que se explicará más adelante.
	-player2: Datos del personaje de la CPU, de tipo personaje_base.
	-n_ronda: Entero que indica el nº de ronda, contenido en esta clase para que
	          conteniese todos los datos necesarios para poder llevar una 
	          partida y decidir su resultado.

*COMENTARIOS: Decidí separar el JSON en 2 y tomar los valores de cada lista por
separado, y para saber la correspondencia insulto/respuesta les añadí un campo 
"pos", el cual incluyo en la clase elem_insulto. Resultaba más sencillo extraer
los valores del JSON al hacerlo de este modo.

-----
elem_insulto: Clase que compone la estructura de un insulto/respuesta. Posee los
atributos:
	-txt: Texto del insulto
	-pos: Posicion del insulto, este campo se emplea para que a la hora de com-
	      -parar los insultos y las respuestas, no tenga que depender de su po-
	      -sición en las listas, de este modo los insultos en un JSON cualquiera
	      pueden estar desordenados, pero la posición correspondiente a su res-
	      puesta debe ser correctamente especificada.

-----
lista_ins_res: Clase auxiliar creada para poder extraer los datos de los JSON. 
Solo posee el campo:
	-insultos[]: Lista de elem_insulto que contiene los varios insultos del JSON.

-----
personaje_base: Clase que define los parámetros de un personaje cualquiera en 
nuestro juego(Jugador o CPU). Sus atributos:
	-estado: Booleano que indica si el jugador está atacando(1) o si está defen-
	         -diendo(0). Empleado en el control de flujo.
	-npc: Booleano que indica si el jugador lo controla el ordenador o no.*
	-eleccion: Insulto seleccionado por el jugador, de tipo elem_insulto.
	-puntuacion: Entero que indica los puntos que tiene un jugador actualmente.

COMENTARIOS: En un principio fué un intento de implementar multijugador desde un
inicio, pero por ahora es una variable de control.
----------------////////////////////////////----------------


----------------Clases funcionales lógicas----------------
Clases que componen la lógica del proyecto y que lo hacen funcionar.
-----
combat_schedule: Clase que controla el flujo de todo el proyecto, está asignada 
al componenente GameManager que gobierna el funcionamiento del juego. Posee los
atributos:
    -bool player1_comienza: Booleano que indica si el jugador humano comienza 
                            atacando o no. Al ser pública, se le asigna su valor
                            desde el inspector.
    -string nombre_archivo_ins: String que indica el nombre del archivo json con
                                insultos.
    -string nombre_archivo_res:String que indica el nombre del archivo json con 
                               respuestas. Al ser públicos tanto este atributo
                               como el anterior se puede cambiar en el inspector,
                               con lo que podemos cambiar al vuelo las listas de
                               insultos y respuestas.
    -int max_ronda: Indica el número máximo de asaltos. Siendo público, se 
                    asigna su valor en el inspector.
    -datos_partida MatchData: Listas, personajes y número de ronda de la partida,
                              debe ser static porque debe compartirse en todos 
                              los turnos.
    -int opciones: Variable que indica el número total de posibles insultos a 
                   escoger por el usuario en cada ronda.
    -int n_insult_resp: Entero de control asociado al número de insultos y, por 
                        tanto, respuestas que hay en el JSON, tomado del valor 
                        Count/Length de las listas de insultos/respuestas_master.
    -List<elem_insulto> dialogo: Lista auxiliar que toma los valores de insultos
                                y respuestas entre los que van a poder escoger
                                los jugadores.
	
    -List<GameObject> boton_dialogo_GO: La lista que contiene las opciones de 
                    respuesta que van a ir apareciendo, los GameObject del pool.
    -ObjectPool botonDialogoPool: El pool de objetos respuesta, que nos permite 
                                  no cargar tanto el recolector de basura.
    -Transform parent_boton_repuestas: Aqui asignaremos el parent de los 
                                    boton_dialogo para que sepan donde aparecer.
    -Text text_directa: Aqui va el texto que saldrá como insulto o respuesta de 
                        ronda del jugador.
    -Text text_enemy: Aqui va el texto que saldrá como insulto o respuesta de 
                      ronda del enemigo.
    -Text texto_npc. Texto que muestra la risa o indiferencia de los NPCs
    -Text Camarero_frase: Texto que muestra la frase del camarero.
    -Text att_deff: Texto que muestra el estado del jugador, ataque o defensa en
                    funcion de la fase.
    -Image victoria: Imagen que aparece cuando gana el jugador.	

Y las funciones:

+LoadFromJson(string nombre_archivo): Carga dede un JSON en una lista los va-
                                     -lores de insultos que en él se encuentren. 

+list_orden(int rango): Nos da una lista ordenada del 0 al némero que queramos.

+pos_rand_list(int[] lista ,int rango): Nos reordena una lista de forma aleatoria
                            y nos da una nueva lista de los x primeros valores.

+lista_n_aleatoria(int tam, int rango): Dar una lista de n valores aleatorios, en
                            un rango[0,x]. Se emplea de auxiliar para ayudar a 
                            la CPU a escoger funciones y para generar sublistas 
                            de insultos y respuestas para las rondas.

+evaluar(elem_insulto insulto, elem_insulto respuesta):Devuelve true en caso de 
                            que las posiciones del insulto y respuesta dados
                            coincidan, y false en el caso contrario.

+buscar_respuesta(int pos_insulto): Nos da el respuesta correspondiente a una 
                                   posicion de insulto dada.

+correspondencia(int pos_giv, List<elem_insulto> lista): Nos dice si en una lista
                                está el insulto/respuesta con la poscion pos_giv.
                                Se usa para determinar si en la lista de respues-
                                -tas aleatorias generadas por lista_defensa_fase()
                                se encuentra la respuesta correcta de la ronda.

+lista_insultos_fase(lista_ins_res ins_o_res): Nos da una lista de elem_insulto
                                               de n valores.

+lista_defensa_fase(int pos_insulto): Nos da una lista de respuestas entre las 
                        cuales se encuentra la correspondiente a a la posición
                        que nos indica el parámetro pos_insulto. Esta lista la 
                        usará aquel jugador que este en estado de defensa en una
                        ronda concreta.

+accion_IA(bool accion):Le asigna a la IA un insulto/respuesta aleatorio para 
                        simular su elección en la ronda actual. En el caso de los
                        insultos se escoge uno cualquiera de la la lista general,
                        en el caso de las respuestas se le proporciona una sub-
                        -lista de n elementos(entre los que está la respuesta 
                        correcta) de los cuales se escoge uno aleatoriamente. 
                        Se puede jugar con el tamaño de las listas más adelante 
                        para implementar modos de dificultad.

+mostrar_dialogo(Text texto, string contenido, bool npc, float secs): Muestra por
                                pantalla el cuadro de diálogo que se desee 
                                activar con el texto que se quiera mostrar. El 
                                booleano npc se utiliza para cambiar el color al
                                texto, de modo que se muestre en rojo en el caso
                                del enemigo y en verde en el caso del jugador. 
                                El float segundos se emplea para indicar cuanto 
                                tiempo queremos que se muestre el texto en pantalla.

+reirse(): Muestra por pantalla el texto que le indica al jugador que no ha habido
          coincidencia entre insulto y respuesta lanzado,siendo esta situacion la
          misma en el caso de la CPU. Se establece como coroutine ya que el texto
          aparece unos segundos y tras ello desaparezca, pero no debe interrumpir
          la ejecución del resto del programa.

+indiferencia(): Muestra por pantalla el texto que le indica al jugador que no
                 ha habido coincidencia entre insulto y respuesta lanzado,
                 siendo esta situacion la misma en el caso de la CPU. Tambien se
                 establece como coroutine por el mismo motivo que en reirse().

+cambiar_estado(): Cambia el texto que indica al jugador en que fase se encuen-
                   -tra(De defensa a ataque y viceversa).

+victoria_partida(): Desencadena el proceso por el cual se le indica al jugador 
                     que ha perdido la partida.

+derrota_partida(): Desencadena el proceso por el cual se le indica al jugador 
                    que ha perdido la partida. 

+quitar_dialogos(): Hace desaparecer los botones en la pantalla para dar mas vi-
                    -sibilidad devolviéndolos al pool de objetos.

+mostrar_opciones(): Genera los botones en pantalla que podemos clicar. Los bo-
                    -tones contendrán insultos o respuestas en función de si 
                    estamos atacando o defendiendonos.

+respuestaClicked(elem_insulto eleccion): La funcion que lleva a cabo las 
                            acciones de asignar la opcion clicada al campo 
                            elección del jugador y evalua quien ha ganado
                            el asalto, dándole fin.

+finAsalto(): Funcion que da lugar al siguiente asalto o al fin del juego en 
              función del nº de ronda.

+finPartida(): Funcion que da lugar al fin de la partida desactivando los textos
            innecesarios de la pantalla y dando paso a la secuencia de victoria/
            derrota, llevándonos finalmente a la pantalla de fin del juego, desde
            la que podemos jugar otra partida o salir del juego. Se ha establecido
            como corutina ya que muestra textos por pantalla x segundos, pero el
            resultado que proporciona(quién ha ganado) lo debe proporcionar antes
            para no interrumpir la ejecución del juego.

+Start(): Inicializado de las variables y del datos_partida que se utilizará 
          durante toda la ejecución del juego.


-----
ObjectPool: Clase que define un 'Pool' de GameObjects,en este caso 
boton_dialogo_bueno para su reciclado durante la ejecución, de este modo los bo-
-tones que aparecen para escoger una opción para el jugador no se crean y 
destruyen, llenando la memoria, si no que se extraen y depositan en este pool, 
cambiando sus datos y contenido mediante el script combat_schedule cada vez que 
se extraen. Posee 2 funciones y 2 atributos:
	-List<GameObject> inactiveInstances: Lista que contiene los actuales 
	                GameObject inactivos almacenados, listos para ser extraídos.
	-GameObject prefab: Variable pública GameObject en la cual asignaremos nues-
	                -tro prefab para que se replique tantas veces como queramos.
	+GetObject(): Función que nos dá uno de los GameObject contenidos en el Pool
	              para poder utilizarlo en nuestro proyecto.
	+ReturnObject(GameObject toReturn): Función que devuelve uno de los 
	            GameObject que estemos utilizando al Pool, eliminándolo de nues-
	            -tra ejecución pero	sin destruirlo.

-----
import_from_json: Clase de prueba creada para probar una serie de funciones 
                  previa inclusión en la lógica del proyecto. Obvio su explica-
                  -ción puesto que las funciones contenidas están presentes en 
                  combat_schedule, además de ser una clase de prueba.

-----
nueva_partida: Clase que contiene la función única cargarNuevaPartida() que acu-
               de al SceneManager y carga la pantalla de juego principal del 
               proyecto.

-----
game_exit: Igual que nueva_partida solo que su función única game_exit() hace 
           que el jugador salga del juego

-----
boton_dialogo_script: Clase que contiene el funcionamiento del botón prefab que 
se presenta en las casillas clicables por el jugador. Contiene 2 funciones y 3 
atributos:
	-combat_schedule combate: Se usa para localizar nuestro combat_schedule y asi
	poder compartirlo cada vez que se generan botones nuevos. En caso contrario 
	se sobreescribiría una y otra vez no dando lugar a un resultado adecuado en 
	el funcionamiento del juego.
	-Text Texto: Variable Text que muestra visualmente el contenido del insulto,
	con lo que el jugador humano puede visualizar lo que va	a escoger.
	-elem_insulto datos: Variable que contiene los datos del insulto mostrado 
	                     por el botón.
	+HandleClick(): Le indica al juego qué hacer al clicar el botón, se le asigna
	                a la función onClick() del botón.
	+Setup(elem_insulto datos): Asigna los datos del insulto elegido aleatoria-
	                  -mente a los datos del botón(esto se realiza en la función
	                  mostrar_opciones() de combat_schedule).
	+Start(): Le asigna al atributo combate el combat_schedule que van a compartir
	          todos los botones, es decir, el combat_schedule de una partida del
	          juego.





ERRORES DE LA PRACTICA
-Al mostrarse la opcion escogida por el rival en la fase de defensa, si 
contestamos mal la siguiente opción de la CPU se muestra de inmediato, cuando 
debería haber unos pocos segundos entre elección del jugador el mostrado del 
siguiente insulto.

-Al mostrarse la opcion escogida por el rival en fase de ataque, la opcion 
escogida por el rival se muestra al instante, cuando no debería ser tan inmediata,
de acuerdo a una perspectiva de juego agradable.

-En el cambio de fase de ataque a defensa, el cuadro de diálogo del rival 
superpone el texto de la última opción escogida sobre la primera, dando la 
sensación de que no ha escogido insulto alguno en la ronda previa.

-Los recuadros de elección del jugador no se escalan correctamente de acuerdo con
la resolución, de modo que en monitores grandes puede resultar incómodo ver el 
texto(aunque no imposible).

-El sprite del enemigo varía en tamaño de la pantalla de juego a la pantalla 
final, además de tener un problema de escalado.









