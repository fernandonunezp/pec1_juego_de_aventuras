﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class boton_dialogo_script : MonoBehaviour {

	public Text texto_dialogo;

    private elem_insulto datos_dialogo;
    private combat_schedule combate;

    // Use this for initialization
    void Start () {
        combate = FindObjectOfType<combat_schedule> ();
    }

    public void Setup(elem_insulto datos){
        datos_dialogo = datos;
        texto_dialogo.text = datos.txt;
    }

    void Update () {

    }

    public void HandleClick(){
        combate.respuestaClicked(datos_dialogo);
    }
}