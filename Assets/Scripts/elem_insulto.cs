﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase auxiliar para agrupar los dos elementos de el JSON
[System.Serializable]		
public class elem_insulto {		
	//Los atributos deben ser públicos ya que van a ser accedidos desde el resto de clases y el GameManager
	public string txt;		//El texto del insulto, que parecerá por pantalla
	public int pos;			//La posicion del insulto en el JSON, que se usará para determinar si es correcto  o no

}
