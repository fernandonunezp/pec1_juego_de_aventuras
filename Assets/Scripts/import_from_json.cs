﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;

//La única función de esta clase es proveer de un intermediario que nos transforme el json en 2 listas
public class import_from_json : MonoBehaviour{

	//Los nombres de los JSON que contienen las listas
	private string ins_FileName = "json_insultos_noalt_directas";
	private string res_FileName = "json_insultos_noalt_respuestas";
	private datos_partida MatchData;
	private int n_insult_resp = 16;
	

	private int opciones = 4; 
	public Text texto_npc;													//Risa o indiferencia de los NPCs

	//Ayuda en la inicialización a generar las listas a partir del JSON
	private lista_ins_res LoadFromJson(string nombre_archivo) {
		//Cargamos la lista de insultos del JSON
		string aux_json = Resources.Load<TextAsset>(nombre_archivo).text;
		//Creamos una lista donde estaran los 16 elem_insult
		lista_ins_res insultos = JsonUtility.FromJson<lista_ins_res>(aux_json);
		return insultos;
	}

	public int[] list_orden(int rango){
		int[] lista_orden= new int[rango];
		for (int i=0; i<rango; i++){
			lista_orden[i]=i;
		}
		return lista_orden;
	}


	//Nos reordena una lista de forma aleatoria y nos da una nueva lista de los x primeros valores
	public int[] pos_rand_list(int[] lista ,int rango){		//tam indica la cantidad de elementos de la lista devuelta, y rango los x primeros números a recibir
		if (rango > lista.Length || rango == 0){			//Si el tamaño que queremos de la nueva lista es mayor que el de la original, tenemos un problema de rango, o si es 0
			Debug.Log("El tamaño pedido excede al de la lista original o es nulo, la lista no se ha alterado");
			return lista;
		}else{	
			//Primero la alteramos por completo
			int aux = 0;
			int pos_a = 0;		//Valores auxiliares
			int pos_b = 0;
			
			while( pos_a < lista.Length){
				//Cogemos un número aleatorio entre 0 y Array.Length
				pos_b = (int)Random.Range(0.0f,(float)lista.Length);
				//Ahora cambiamos los valores de la lista(la posicion en la que estamos y el otro) con un Swap()
				aux = lista[pos_a];				//Metemos en un auxiliar el valor de la posicion original
				lista[pos_a] = lista[pos_b];	//Cambiamos el valor de donde estamos por el elegido
				lista[pos_b] = aux;				//Ponemos en el otro lado el valor del anterior

				pos_a++;					//Siguiente número						
			}
			if (rango == lista.Length){		//Si coincide el rango con la lista, nos devuelve al propia lista modificada
				return lista;
			}else{	
				//Y ahora metemos los x primeros valores en una nueva lista
				int[] nueva_lista = new int[rango];
				for (int i=0;i<rango;i++){
					nueva_lista[i]=lista[i];
				}
				return nueva_lista;
			}
		}	
	}	

	//Dar una lista de n valores aleatorios, en un rango[0,x]
	public int[] lista_n_aleatoria(int tam, int rango){
		int[] aux = list_orden(rango); 	//primero una lista de x valores
		return pos_rand_list(aux,tam);	//Devolvemos la lista aleatoria
	}

	//Devuelve la comprobación de una respuesta correcta o incorrecta
	public bool evaluar(elem_insulto insulto, elem_insulto respuesta){
		return (insulto.pos == respuesta.pos);
	}

	//Nos da la respuesta correspondiente a una posicion de insulto dada
	public elem_insulto buscar_respuesta(int pos_insulto){
		//Buscamos en la lista de respuestas el que tenga el pos de pos_insulto
		//Si no encuentra el insulto por un fallo en al construcción del json, devolvemos un insulto que es el correcto pero con un texto no correcto
		elem_insulto respuesta_adecuada = new elem_insulto();
		respuesta_adecuada.txt = "ERROR: Insulto no encontrado, este es el insulto correcto";
		respuesta_adecuada.pos = pos_insulto;
		for (int i = 0; i < n_insult_resp; i++){
			if (pos_insulto == MatchData.lista_respuestas_master.insultos[i].pos){
				respuesta_adecuada = MatchData.lista_respuestas_master.insultos[i];			//Si la encuentra la asigna
				break;																		//Salimos del bucle por no perder el tiempo
			}
		}
		return respuesta_adecuada; 

	}

	//Nos dice si en una lista está la respuesta correspondiente a x posicion
	public bool correspondencia(int pos_giv, List<elem_insulto> lista){		//Le pasamos los valores de posición(elem_insulto) y la lista donde queremos ver si está
		//Creamos el dialog_fase base que se devolverá
		bool correspondiente = false;						//El bool auxiliar
		//Ahora vemos si está
		for (int aux = 0; aux < lista.Count; aux++){			//Recorremos toda la lista de respuestas
			if (pos_giv == lista[aux].pos){						//Si encuentra la correspondencia	
				correspondiente = true;						//La variable de control se pone a verdadero
			}
		}
		return correspondiente;											//Devolvemos si la ha encontrado o no
	}


	//Nos da una serie de 'n' insultos/respuestas para escoger, el número de estas determinado por la variable opciones
	public List<elem_insulto> lista_insultos_fase(lista_ins_res ins_o_res){	//Especificamos nº de opciones que queramos, y la lista de la que sacamos opciones
		//Cogemos una lista aleatoria de POSICIONES
		int[] aux_lista = lista_n_aleatoria(opciones,n_insult_resp);
		//Con las posiciones nos hacemos una lista de insultos
		List<elem_insulto> lista_opciones = new List<elem_insulto>();	//Creamos la lista de elem_insulto que vamos a emplear, del tamaño adaptado a lo indicado por opciones
		//En 'lista_opciones' se introducen los insultos en funcion de las posiciones dadas por la lista aleatoria de arriba
		for (int i = 0; i < opciones; i++){
			lista_opciones.Add(ins_o_res.insultos[aux_lista[i]]); 	//Estamos cogiendo de la lista aleatoria de posiciones el valor correspondiente a la primera posicion de esa lista,			
																			//consiguiendo asi obtener el insulto correspondiente de forma aleatoria.
		}
		return lista_opciones;
	}


	//Nos genera el insulto y la lista de respuestas asociada a ese insulto
	public List<elem_insulto> lista_defensa_fase(int pos_insulto){
		//Aprovechamos la función de antes para hacer un List<elem_insulto>, pero con la lista de respuestas maestra
		List<elem_insulto> defensas = lista_insultos_fase(MatchData.lista_respuestas_master);		//La lista se encuentra en MatchData, por lo que debemos acceder a ella
		//Ahora comprabamos si en la lista aleatoria creada está ya la respuesta
		if (!correspondencia(pos_insulto, defensas)){									//Si no encuentra la respuesta correcta, buscamos la respuesta correcta y la introducimos
			int[] aux_posiciones = lista_n_aleatoria(1,opciones);		//Cogemos una posición al azar del 0 a opciones(=5)
			defensas[aux_posiciones[0]] = buscar_respuesta(pos_insulto);		//Metemos en una posición aleatoria la respuesta correcta
		}
		return defensas;
	}


	//Muestra en el cuadro dialogo el texto que queramos
	public IEnumerator mostrar_dialogo(Text texto, string contenido, bool npc){
		//Cambiamos el color en funcion de quien hable
		if (npc){
			texto.color = new Color32(255, 59, 0, 255);	//Rojo anaranjado	
		}else{
			texto.color = new Color32(25, 162, 6, 255);	//Verde MI
		}
		//Indicamos los segundos que queremos que aprezca el texto 		
		texto.text = contenido; 				//le ponemos el texto que queramos
		texto.enabled = true;			//activamos el componente
		yield return new WaitForSeconds(2f);
		//Dependiendo de si habla o no el enemigo
		texto.enabled = false;			
	}//Si hablamos nosotros, no mantenemos el texto, si habla el enemigo, lo mantenemos para poder responder







	public void LoadgameData() {

		//Testeo listas insultos
		MatchData = new datos_partida();
		//Cargamos el MatchData con todo lo necesario
		MatchData.lista_insultos_master = LoadFromJson(ins_FileName);
		MatchData.lista_respuestas_master = LoadFromJson(res_FileName);

		//Decidimos el que empieza, con exlusion mutua para que no haya conflicto de turnos
		if (true){
			MatchData.player1 = new personaje_base(false, true);
			MatchData.player2 = new personaje_base(true, false);
		}else{
			MatchData.player1 = new personaje_base(false, false);
			MatchData.player2 = new personaje_base(true, true);			
		}

		//Empezamos en la ronda 1
		MatchData.n_ronda = 1;	
		
		//Vemos cuantos insultos hay en la lista de ins/res
		Debug.Log("longitud");
		n_insult_resp = MatchData.lista_insultos_master.insultos.Length;									
		Debug.Log(MatchData.lista_insultos_master.insultos.Length);


		//Cargamos la lista de insultos del JSON
		string aux_json_ins = Resources.Load<TextAsset>(ins_FileName).text;
		string aux_json_res = Resources.Load<TextAsset>(res_FileName).text;
		//Creamos una lista donde estaran los 16 elem_insult
		lista_ins_res insultos = JsonUtility.FromJson<lista_ins_res>(aux_json_ins);
		lista_ins_res respuestas = JsonUtility.FromJson<lista_ins_res>(aux_json_res);
		Debug.Log(aux_json_ins);
		Debug.Log(aux_json_res);
		//Introducimos lo del JSON en la lista
		Debug.Log(insultos);
		Debug.Log(insultos.insultos[0]);
		Debug.Log(insultos.insultos[0].txt);
		Debug.Log(insultos.insultos[0].pos);
				

		Debug.Log("ESPACIO_TESTEO");

		Debug.Log("Insulto auxiliar");
		List<elem_insulto> lista_insultos_prueba = new List<elem_insulto>();
		lista_insultos_prueba = lista_insultos_fase(insultos);
		Debug.Log(lista_insultos_prueba);
		Debug.Log(lista_insultos_prueba.Count);
		for (int i=0; i<lista_insultos_prueba.Count; i++){
			Debug.Log(lista_insultos_prueba[i].txt);
			Debug.Log(lista_insultos_prueba[i].pos);
		}
		int[] posic = lista_n_aleatoria(1,n_insult_resp);		//Insulto aleatorio a coger
		Debug.Log("Posicion");
		Debug.Log(posic[0]);
		lista_insultos_prueba = lista_defensa_fase(posic[0]);
		Debug.Log(lista_insultos_prueba);
		Debug.Log(lista_insultos_prueba.Count);
		for (int i=0; i<lista_insultos_prueba.Count; i++){
			Debug.Log(lista_insultos_prueba[i].txt);
			Debug.Log(lista_insultos_prueba[i].pos);
		}
		Debug.Log("ESPACIO TESTEO FIN");

		texto_npc.text = "Prueba de texto";													//Risa o indiferencia de los NPCs
		//texto_npc.enabled = false;
		StartCoroutine(mostrar_dialogo(texto_npc, "cambiando",true));
		texto_npc.text = "Prueba de texto";													//Risa o indiferencia de los NPCs



/*
		Debug.Log("lista ataques");
		elem_insulto[] aux = new elem_insulto[opciones];
		Debug.Log(aux);
		Debug.Log("aqui_la tengo");

		aux = lista_insultos_fase(insultos);
		Debug.Log(aux);
		Debug.Log(aux[0]);

		Debug.Log("FALLO");

		aux = lista_insultos_fase(MatchData.lista_insultos_master);
		foreach (elem_insulto n in aux){
			Debug.Log(n.txt);
			Debug.Log(n.pos);
		}
*/

	}
}