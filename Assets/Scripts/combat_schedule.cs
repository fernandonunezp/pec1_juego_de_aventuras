using System.Collections;		//Necesario para las listas
using System.Collections.Generic;		//Necesario para las listas
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;		//Necesario para los JSON



public class combat_schedule : MonoBehaviour {

	//Variables modificables en el inspector
	//Chequeo de turnos
	public bool player1_comienza = true;
	public string nombre_archivo_ins = "json_insultos_noalt_directas";				//Nombre del json con insultos
	public string nombre_archivo_res = "json_insultos_noalt_respuestas";			//Nombre del json con respuestas
	public int max_ronda;				//Número de asaltos

	//Variables internas
	public static datos_partida MatchData = new datos_partida();		//Listas, personajes y número de ronda todo en uno, debe ser static porque dura toda la partida.
																		//y la comparten todas las instancias de boton_dialogo.
	private int opciones = 4;				//Variable de control de número total de posibles insultos a escoger por el usuario
	private int n_insult_resp;
	private List<elem_insulto> dialogo = new List<elem_insulto>();
	
	//Variables UI
	private List<GameObject> boton_dialogo_GO = new List<GameObject>();  	//La lista que contiene a las opciones de respuesta que van a ir apareciendo
	public ObjectPool botonDialogoPool; 									//El pool de objetos respuesta, que nos permite no cargar tanto el recolector de basura.
	public Transform parent_boton_repuestas;								//Aqui asignaremos el padre de los botones de respuestas
	public Text text_directa;												//Aqui va el texto que saldrá como insulto o respuesta de ronda
	public Text text_enemy;												//Texto del enemigo
	public Text texto_npc;													//Risa o indiferencia de los NPCs
	public Text Camarero_frase;												//Frase del camarero
	public Text att_deff;													//Estado del jugador, ataque o defensa en funcion de la fase
	public Image victoria;													//Imagen que nos dice que hemos ganado

	////////////////////////////////////////////////////////////////////
	////////////////////////////AUXILIARES//////////////////////////////
	////////////////////////////////////////////////////////////////////

	//Ayuda en la inicialización a generar las listas a partir del JSON
	private lista_ins_res LoadFromJson(string nombre_archivo) {
		//Cargamos la lista de insultos del JSON
		string aux_json = Resources.Load<TextAsset>(nombre_archivo).text;
		//Creamos una lista donde estaran los 16 elem_insult
		lista_ins_res insultos = JsonUtility.FromJson<lista_ins_res>(aux_json);
		return insultos;
	}


	//Nos da una lista ordenada del 0 al rango que queramos
	public int[] list_orden(int rango){
		int[] lista_orden = new int[rango];
		for (int i=0; i<rango; i++){
			lista_orden[i]=i;
		}
		return lista_orden;
	}


	//Nos reordena una lista de forma aleatoria y nos da una nueva lista de los x primeros valores
	public int[] pos_rand_list(int[] lista ,int rango){		//tam indica la cantidad de elementos de la lista devuelta, y rango los x primeros números a recibir
		if ((rango > lista.Length) || rango == 0){			//Si el tamaño que queremos de la nueva lista es mayor que el de la original, tenemos un problema de rango
			Debug.LogError("El tamaño pedido excede al de la lista original, la lista no se ha alterado");
			return lista;
		}else{	
			//Primero la alteramos por completo
			int aux = 0;
			int pos_a = 0;		//Valores auxiliares
			int pos_b = 0;
			
			while( pos_a < lista.Length){
				//Cogemos un número aleatorio entre 0 y Array.Length
				pos_b = (int)Random.Range(0.0f,(float)lista.Length);
				//Ahora cambiamos los valores de la lista(la posicion en la que estamos y el otro) con un Swap()
				aux = lista[pos_a];				//Metemos en un auxiliar el valor de la posicion original
				lista[pos_a] = lista[pos_b];	//Cambiamos el valor de donde estamos por el elegido
				lista[pos_b] = aux;				//Ponemos en el otro lado el valor del anterior

				pos_a++;					//Siguiente número						
			}
			if (rango == lista.Length){		//Si coincide el rango con la lista, nos devuelve al propia lista modificada
				return lista;
			}else{	
				//Y ahora metemos los x primeros valores en una nueva lista
				int[] nueva_lista = new int[rango];
				for (int i=0;i<rango;i++){
					nueva_lista[i]=lista[i];
				}
				return nueva_lista;
			}
		}
	}
	

	//Dar una lista de n valores aleatorios, en un rango[0,x]
	public int[] lista_n_aleatoria(int tam, int rango){
		int[] aux = list_orden(rango); 	//primero una lista de x valores
		return pos_rand_list(aux,tam);	//Devolvemos la lista aleatoria
	}


	////////////////////////////////////////////////////////////////////
	//////////////////////AUXILIARES JUEGO//////////////////////////////
	////////////////////////////////////////////////////////////////////

	//Devuelve la comprobación de una respuesta correcta o incorrecta
	public bool evaluar(elem_insulto insulto, elem_insulto respuesta){
		return (insulto.pos == respuesta.pos);
	}


	//Nos da la respuesta correspondiente a una posicion de insulto dada
	public elem_insulto buscar_respuesta(int pos_insulto){
		//Buscamos en la lista de respuestas el que tenga el pos de pos_insulto
		//Si no encuentra el insulto por un fallo en al construcción del json, devolvemos un insulto que es el correcto pero con un texto no correcto
		elem_insulto respuesta_adecuada = new elem_insulto();
		respuesta_adecuada.txt = "ERROR: Insulto no encontrado, este es el insulto correcto";
		respuesta_adecuada.pos = pos_insulto;
		for (int i = 0; i < n_insult_resp; i++){
			if (pos_insulto == MatchData.lista_respuestas_master.insultos[i].pos){
				respuesta_adecuada = MatchData.lista_respuestas_master.insultos[i];			//Si la encuentra la asigna
				break;																		//Salimos del bucle por no perder el tiempo
			}
		}
		return respuesta_adecuada; 

	}


	//Nos dice si en una lista está la respuesta correspondiente a la posicion dada
	public bool correspondencia(int pos_giv, List<elem_insulto> lista){		//Le pasamos los valores de posición(elem_insulto) y la lista donde queremos ver si está
		bool correspondiente = false;						//El que se devolverá, asumimos que de entrada no está el insulto
		//Ahora vemos si está
		for (int aux = 0; aux < lista.Count; aux++){			//Recorremos toda la lista de respuestas
			if (pos_giv == lista[aux].pos){						//Si encuentra la correspondencia	
				correspondiente = true;						//La variable de control se pone a verdadero
			}
		}
		return correspondiente;											//Devolvemos si la ha encontrado o no
	}


	//Nos da una lista de elem_insulto
	public List<elem_insulto> lista_insultos_fase(lista_ins_res ins_o_res){	//Especificamos nº de opciones que queramos, y la lista de la que sacamos opciones
		//Cogemos una lista aleatoria de POSICIONES
		int[] aux_lista = lista_n_aleatoria(opciones,n_insult_resp);
		//Con las posiciones nos hacemos una lista de insultos
		List<elem_insulto> lista_opciones = new List<elem_insulto>();	//Creamos la lista de elem_insulto que vamos a emplear, del tamaño adaptado a lo indicado por opciones
		//En 'lista_opciones' se introducen los insultos en funcion de las posiciones dadas por la lista aleatoria de arriba
		for (int i = 0; i < opciones; i++){
			lista_opciones.Add(ins_o_res.insultos[aux_lista[i]]); 	//Estamos cogiendo de la lista aleatoria de posiciones el valor correspondiente a la primera posicion de esa lista,			
																			//consiguiendo asi obtener el insulto correspondiente de forma aleatoria.
		}
		return lista_opciones;
	}


	//Nos da una lista de respuestas con una de ellas correspondiente a la posicion proporcionada y el resto aleatorias
	public List<elem_insulto> lista_defensa_fase(int pos_insulto){
		//Aprovechamos la función de antes para hacer un List<elem_insulto>, pero con la lista de respuestas maestra
		List<elem_insulto> defensas = lista_insultos_fase(MatchData.lista_respuestas_master);		//La lista se encuentra en MatchData, por lo que debemos acceder a ella
		//Ahora comprabamos si en la lista aleatoria creada está ya la respuesta
		if (!correspondencia(pos_insulto, defensas)){									//Si no encuentra la respuesta correcta, buscamos la respuesta correcta y la introducimos
			int[] aux_posiciones = lista_n_aleatoria(1,opciones);		//Cogemos una posición al azar del 0 a opciones(=5)
			defensas[aux_posiciones[0]] = buscar_respuesta(pos_insulto);		//Metemos en una posición aleatoria la respuesta correcta
		}
		return defensas;
	}
	

	////////////////////////////////////////////////////////////////////
	//////////////////////LOGICA DEL JUEGO//////////////////////////////
	////////////////////////////////////////////////////////////////////

	//Ataque o defensa de la accion_ia
	public void accion_IA(bool accion){
		//Cogemos un insulto al azar de la lista general
		if(accion){
			int[] decision = lista_n_aleatoria(1,n_insult_resp);				//LA IA puede coger un insulto al azar de entre todos, para el jugador hay que restringir el espacio
			MatchData.player2.eleccion = MatchData.lista_insultos_master.insultos[decision[0]];
		}else{
			int[] decision = lista_n_aleatoria(1,opciones);						//Cogemos una valor aleatorio del 0 a opciones, de ese modo la IA escoge un valor aleatorio de entre varios
			MatchData.player2.eleccion = lista_defensa_fase(MatchData.player1.eleccion.pos)[decision[0]];
		}
		System.Threading.Thread.Sleep(1000);
		StartCoroutine(mostrar_dialogo(text_enemy, MatchData.player2.eleccion.txt, true, 7.0f));	
	}


	////////////////////////////////////////////////////////////////////
	///////////////////////////ACCIONES UI//////////////////////////////
	////////////////////////////////////////////////////////////////////


	//Muestra en el cuadro dialogo el texto que queramos
	public IEnumerator mostrar_dialogo(Text texto, string contenido, bool npc, float secs){
		//Cambiamos el color en funcion de quien hable
		if (npc){
			texto.color = new Color32(255, 59, 0, 255);	//Rojo anaranjado
		}else{
			texto.color = new Color32(25, 162, 6, 255);	//Verde MI
		}
		//Indicamos los segundos que queremos que aprezca el texto
		texto.text = contenido; 				//le ponemos el texto que queramos
		//Mostramos el texto 
		texto.enabled = true;
		yield return new WaitForSeconds(secs);
		texto.enabled = false; //Si es la CPU, no desactiva el texto
	}


	//Muestra el texto de personajes riéndose
	public IEnumerator reirse(){
		//Play audio
		//Quitamos el dialogo
		//text_directa.enabled = false;
		//Ahora aparecen los de los npc
		texto_npc.text = "JA JA JA";
		Camarero_frase.text = "WOW";
		texto_npc.enabled = true;
		Camarero_frase.enabled = true;
		yield return new WaitForSeconds(2.5f);
		Camarero_frase.enabled = false;
		texto_npc.enabled = false;
	}


	//Muestra el texto de personajes estoicos
	public IEnumerator indiferencia (){
		//Quitamos el dialogo
		//text_directa.enabled = false;
		//Ahora aparecen los de los npc
		texto_npc.text = "...   ...";
		texto_npc.enabled = true;
		yield return new WaitForSeconds(2.5f);
		Camarero_frase.enabled = false;
		texto_npc.enabled = false;
	}


	//Cambia el estado del texto estado al opuesto
	public void cambiar_estado(){
		if (MatchData.player1.estado){
			att_deff.text = "Ataque";
		}else{
			att_deff.text = "Defensa";
		}
	}


	//Proceso de finalizado con victoria
	public void victoria_partida(){
		Camarero_frase.text = "A esta quedas invitado";
    	Camarero_frase.enabled = true;
		//Sonido aplausos
		victoria.enabled = true;
	}


	//Proceso de finalizado con derrota
	public void derrota_partida(){
		Camarero_frase.text = "Casi mejor ve marchándote, campeón";
    	Camarero_frase.enabled = true;
	}


	//Quita los botones de la ronda anterior
	private void quitar_dialogos(){
        while (boton_dialogo_GO.Count > 0){
            botonDialogoPool.ReturnObject(boton_dialogo_GO[0]);
            boton_dialogo_GO.RemoveAt(0);
        }
    }


	private void mostrar_opciones(){
        //Comprobamos si atacamos o respondemos
        if (MatchData.player1.estado){//Si atacamos	
        	dialogo = lista_insultos_fase(MatchData.lista_insultos_master); 	//Cogemos una lista de insultos cualquiera que se mostrará
        }else{ 	//Si defendemos
        	//La IA debe coger un insulto
        	accion_IA(MatchData.player2.estado);													//La IA coge un insulto
        	dialogo = lista_defensa_fase(MatchData.player2.eleccion.pos); 		//Creamos una lista de respuestas en función de la posición de la opcion del P2
        }
        //Creamos un objeto que albergue las opciones de ahora, preguntas o respuestas     
        for (int i = 0; i < opciones; i++){										//opciones nos da el numero de botones que debemos coger(crear)
            GameObject boton_dialogo = botonDialogoPool.GetObject();			//Cogemos el boton(gameobject) del pool
            boton_dialogo.transform.SetParent(parent_boton_repuestas,false);	//Le ponemos como padre la zona opciones, que es donde va a aparecer
            boton_dialogo_GO.Add(boton_dialogo);								//Lo añadimos a la lista de los que van a aparecer

            boton_dialogo_script opcion_dialogo = boton_dialogo.GetComponent<boton_dialogo_script>(); 	//Ahora le metemos a cada boton_dialogo el insulto correspondiente
            opcion_dialogo.Setup(dialogo[i]);											//Le ponemos un insulto de la lista de arriba a cada opcion
        }
	}


	public void respuestaClicked(elem_insulto eleccion){
		//Quitamos los cuadros de diálogo
		quitar_dialogos();
		//Asignamos el valor de lo clicado a nuestro campo elección		
		MatchData.player1.eleccion = eleccion;
		//Hacemos que nuestra respuesta/insulto salga por pantalla
		StartCoroutine(mostrar_dialogo(text_directa, eleccion.txt, false, 1.5f));
		//System.Threading.Thread.Sleep(1000);
		//Si atacamos
		if (MatchData.player1.estado){
			//La IA debe responder para poder evaluarse su respuesta 
			accion_IA(MatchData.player2.estado);
		}	//En caso de que defendiesemos la IA ya habría realizado su elección en mostrar_opciones()
		
		//El insulto/respuesta escogido por player1, NOSOTROS, hay que compararlo con la opcion cogida por player2, EL PC. 
		if (MatchData.player1.estado) {							//Si atacamos
			if(evaluar(MatchData.player1.eleccion, MatchData.player2.eleccion)){		//Y coinciden elecciones
				//Animacion
				StartCoroutine(reirse());
				MatchData.player2.puntuacion++;											//Perdemos
				MatchData.player1.estado = !MatchData.player1.estado;					//Si fallamos el otro pasa al ataque
				cambiar_estado();														//Cambiamos el valor de la etiqueta de estado
				MatchData.player2.estado = !MatchData.player1.estado;											 
			}else{
				//Animacion
				StartCoroutine(indiferencia());
				MatchData.player1.puntuacion++;
			}
		}else{																			//Si defendemos
			if(evaluar(MatchData.player1.eleccion, MatchData.player2.eleccion)){		//Y coinciden
				//Animacion
				text_enemy.enabled = false;
				StartCoroutine(reirse());			
				MatchData.player1.puntuacion++;											//Se añade un punto al rival 
				MatchData.player1.estado = !MatchData.player1.estado;					
				cambiar_estado();
				MatchData.player2.estado = !MatchData.player2.estado;
			}else{
				StartCoroutine(indiferencia());
				MatchData.player2.puntuacion++;											//Ganamos 1 punto!
			}
		}
		finAsalto();	
    }


     public void finAsalto(){
     	if (max_ronda == MatchData.n_ronda){
     		StartCoroutine(finPartida());
     	}else{
     		MatchData.n_ronda++;
     		mostrar_opciones();
     	}   	
    }

    public IEnumerator finPartida(){
    	att_deff.enabled = false;					//Quitamos los textos de turno y de dialogo
    	text_directa.enabled = false;
    	text_enemy.enabled = false;
    	StopCoroutine(reirse());		//Si alguna de las 2 esta funcionando la detenemos para que no interfiera con la secuencia final
    	StopCoroutine(indiferencia());
    	//Comprobamos quien tiene mas puntos
    	if(MatchData.player1.puntuacion < MatchData.player2.puntuacion){	//Si perdemos
    		derrota_partida();
    	}else{																//Si ganamos
			victoria_partida();
    	}
		//Final
    	Debug.Log("Fin del juego");
    	yield return new WaitForSeconds(2);
		SceneManager.LoadScene(2);
    }



	//Inicializado de todo
	void Start(){
		//Cargamos el MatchData con todo lo necesario
		MatchData.lista_insultos_master = LoadFromJson(nombre_archivo_ins);
		MatchData.lista_respuestas_master = LoadFromJson(nombre_archivo_res);

		//Decidimos el que empieza, con exlusion mutua para que no haya conflicto de turnos
		if (player1_comienza){
			MatchData.player1 = new personaje_base(false, true);
			MatchData.player2 = new personaje_base(true, false);
		}else{
			MatchData.player1 = new personaje_base(false, false);
			MatchData.player2 = new personaje_base(true, true);			
		}

		//Empezamos en la ronda 1
		MatchData.n_ronda = 1;	
		
		//Vemos cuantos insultos hay en la lista de ins/res
		n_insult_resp = MatchData.lista_insultos_master.insultos.Length;									
		cambiar_estado();
		//Comenzamos el juego
		mostrar_opciones();
	}

	void Update(){

	}

}
