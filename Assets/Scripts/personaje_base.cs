using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Características comunes tanto a jugador como a enemigo
[System.Serializable]
public class personaje_base  {

	//Los atributos son públicos ya que deben ser accedidos por el scrip principal GameManager
	public bool npc;									//Con esto distinguimos entre jugador y ordenador
	public bool estado; 								//Ataque(false) o defensa(true)
	public int puntuacion; 								//Para determinar la victoria
	public elem_insulto eleccion; 						//Posicion del insulto/respuesta escogida para crear el resultado


	public personaje_base(bool npc, bool estado){
		this.npc = npc;
		this.estado = estado;
		this.puntuacion = 0;
		this.eleccion = new elem_insulto();
	}
}


