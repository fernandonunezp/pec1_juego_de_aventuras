﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase auxiliar para coger los datos del json, se instancia una sola vez y se trabaja con ella
[System.Serializable]
public class lista_ins_res  {

	public elem_insulto[] insultos;

}
