﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//esta clase contiene todos los datos necesarios para que se desarrolle el juego en su total ejecución.
public class datos_partida {

	public lista_ins_res lista_insultos_master;				//Lista de insultos(campos: .insultos)
	public lista_ins_res lista_respuestas_master;				//Lista de respuestas
	
	public personaje_base player1;								//Personaje 1 del juego(campos: .npc .estado .eleccion .puntuacion)
	public personaje_base player2;								//Personaje 2 del juego(SIEMPRE UN NPC)

	public int n_ronda;					//Variable que indica el nº de ronda
	public int n_insult_resp;			//Variable de control de número total de insultos

}

